<?php

use Bitrix\Main\Localization\Loc; // Подключаем сообщения
use Bitrix\Main\ModuleManager; // Преднозначен для установки, удаления, регистрации модуля
use Bitrix\Main\Config\Option; //Для настройки опций модуля например включение, отключения модуля, удаления
use Bitrix\Main\EventManager; //Для регистрации и управление событиями
use Bitrix\Main\Application; //Подключения к APPLICATION
use Bitrix\Main\IO\Directory; // Для работы с директориями и файлами

Loc::loadMessages(__FILE__);

class button extends CModule
{


    // =================================================================
    public function __construct()
    {
        if (file_exists(__DIR__ . '/version.php')) {
            $arModeleVersion = array();
            include_once __DIR__ . '/version.php';
            // записываем что будет при описании модуля 
            $this->MODULE_ID = get_class();
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
            $this->MODULE_NAME = Loc::GetMessage('FALBAR_TOTOP_NAME');
            $this->MODULE_DESCRIPTION = Loc::GetMessage('FALBAR_TOTOP_DESCRIPTION');
            $this->PARTNER_NAME = Loc::GetMessage('FALBAR_TOTOP_PARTNER_NAME');
        }
        return false;
    }
    // =================================================================

    // Метод установки МОДУЛЯ
    function DoInstall()
    {

        global $APPLICATION;
        // CheckVersion- метод сравнивает 2 версии
        if (CheckVersion(ModuleManager::getversion("main"), "14.00.00")) {

            $this->InstallFiles(); // Уставновка файлов
            $this->InstallDB(); // Cоздание таблиц

            ModuleManager::registerModule($this->MODULE_ID); //Метод резистрирует модуль в системе, что позволит обращатся к нему

            $this->InstallEvents(); //используется для регистрации обработчиков событий и установки некоторых событий
        } else {
            //ThrowException- Метод выводит сообщение об ошибке
            $APPLICATION->ThrowException(
                Loc::getMessage('FALBAR_TOTOP_INSTALL_ERROR_VERSION')
            );
        }
        // IncludeAdminFile - Подключает скрипт с административным прологом и эпилогом
        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("FALBAR_TOTOP_INSTALL_TITLE") . "\"" . Loc::getMessage("FALBAR_TOTOP_NAME") . "\"",
            __DIR__ . "/step.php"
        );
    }
    // =================================================================

    public function InstallFiles()
    {
        CopyDirFiles(
            __DIR__ . "/assets/script", // от куда будут копироваться файлы
            Application::getDocumentRoot() . "bitrix/js" . $this->MODULE_ID . "/", // куда будут копироваться файлы
            //рекурсивно
            true,
            true

        );
        CopyDirFiles(
            __DIR__ . "/assets/style",
            Application::getDocumentRoot() . "bitrix/css" . $this->MODULE_ID . "/",
            true,
            true

        );
        return false;
    }
    // =================================================================

    //Не использую базу данных
    public function installDB()
    {
        return false;
    }
    // =================================================================

    /*При определенном событии (завершении вывода контента на страницу) нужно выполнить определенный код вашего модуля.*/
    public function InstallEvents()
    {
        //EventManager - паттерн Singleton, обращатся через getInstance()
        EventManager::getInstance()->registerEventHandler(
            "main",
            "OnBeforeEndBufferContent",
            $this->MODULE_ID,
            "Falbar\ToTop\Main", //Это имя класса, который будет обрабатывать событие. В данном случае, класс "Falbar\ToTop\Main" должен содержать метод, который будет выполняться при срабатывании события.
            "appendScriptsToPage" //Это имя метода класса "Falbar\ToTop\Main", который будет вызываться при срабатывании события.
        );
        return false;
    }
    // =================================================================

    // Метод удаления МОДУЛЯ
    public function DoUninstall()
    {
        global $APPLICATION;
        $this->UnInstallFiles(); // Удаление файлов
        $this->UnInstallDB(); // Удаление таблиц
        $this->UnInstallEvents();

        ModuleManager::unRegisterModule($this->MODULE_ID); //Метод резистрирует модуль в системе, что позволит обращатся к нему


        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("FALBAR_TOTOP_UNINSTALL_TITLE") . " \"" . Loc::getMessage("FALBAR_TOTOP_NAME") . "\"",
            __DIR__ . "/unstep.php"
        );

        return false;
    }

    // =================================================================

    public function UnInstallFiles()
    {

        Directory::deleteDirectory(
            Application::getDocumentRoot() . "/bitrix/js/" . $this->MODULE_ID
        );

        Directory::deleteDirectory(
            Application::getDocumentRoot() . "/bitrix/css/" . $this->MODULE_ID
        );

        return false;
    }

    // =================================================================
    public function UnInstallDB()
    {

        Option::delete($this->MODULE_ID);
        return false;
    }

    // =================================================================
    public function UnInstallEvents()
    {

        EventManager::getInstance()->unRegisterEventHandler( //Удаляет регистрационную запись обработчика события.
            "main",
            "OnBeforeEndBufferContent",
            $this->MODULE_ID,
            "Falbar\ToTop\Main",
            "appendScriptsToPage"
        );

        return false;
    }
}
