<?php

$MESS["FALBAR_TOTOP_NAME"]           = "Мой модуль";
$MESS["FALBAR_TOTOP_DESCRIPTION"]  = "Добавляет кнопки соц сетей на сайт.";
$MESS["FALBAR_TOTOP_PARTNER_NAME"] = "SofIla";
$MESS["FALBAR_TOTOP_PARTNER_URI"]  = "http://falbar.ru/";
$MESS["FALBAR_TOTOP_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["FALBAR_TOTOP_INSTALL_TITLE"]        = "Установка модуля";
$MESS["FALBAR_TOTOP_UNINSTALL_TITLE"] = "Деинсталляция модуля";
$MESS["FALBAR_TOTOP_STEP_BEFORE"]        = "Модуль";
$MESS["FALBAR_TOTOP_STEP_AFTER"]         = "установлен";
$MESS["FALBAR_TOTOP_STEP_SUBMIT_BACK"] = "Вернуться в список";
$MESS["FALBAR_TOTOP_UNSTEP_BEFORE"]      = "Модуль";
$MESS["FALBAR_TOTOP_UNSTEP_AFTER"]       = "удален";
$MESS["FALBAR_TOTOP_UNSTEP_SUBMIT_BACK"] = "Вернуться в список";
