<?
//  Уставка модуля
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

//check_bitrix_sessid -проверяет сесию
if (!check_bitrix_sessid()) {

    return;
}

if ($errorException = $APPLICATION->GetException()) {
    // показывает сообщение об исключении 
    echo (CAdminMessage::ShowMessage($errorException->GetString()));
} else {
    // если все хорошо то показывается сообщение об успешной установки
    // CAdminMessage::ShowNote - для показа сообщений в административной части
    echo (CAdminMessage::ShowNote(Loc::getMessage("FALBAR_TOTOP_STEP_BEFORE") . " " . Loc::getMessage("FALBAR_TOTOP_STEP_AFTER")));
}
?>

<form action="<? echo ($APPLICATION->GetCurPage()); ?>">
    <input type="hidden" name="lang" value="<? echo (LANG); ?>" />
    <input type="submit" value="<? echo (Loc::getMessage("FALBAR_TOTOP_STEP_SUBMIT_BACK")); ?>">
</form>