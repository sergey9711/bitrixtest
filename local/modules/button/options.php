<?
// пространство имен для подключений ланговых файлов
use Bitrix\Main\Localization\Loc;
// пространство имен для получения ID модуля
use Bitrix\Main\HttpApplication;
// пространство имен для загрузки необходимых файлов, классов, модулей
use Bitrix\Main\Loader;
// пространство имен для работы с параметрами модулей хранимых в базе данных
use Bitrix\Main\Config\Option;
// подключение ланговых файлов
Loc::loadMessages(__FILE__);
// получаем id модуля

$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = 'button';
// подключение модуля
Loader::includeModule($module_id);
// настройки модуля для админки в том числе значения по умолчанию
$aTabs = array(
    array(
        // значение будет вставленно во все элементы вкладки для идентификации
        "DIV" => "edit",
        // название вкладки в табах 
        "TAB" => "Настройки",
        // название вкладки в админке
        "TITLE" => "Общие",
        // массив с опциями секции
        "OPTIONS" => array(
            "Список кнопок",
            array(
                // имя элемента формы
                "hmarketing_checkbox",
                // поясняющий текст
                "Поясняющий текс элемента checkbox",
                // значение checkbox по умолчанию "Да"
                "Y",
                // тип элемента формы "checkbox"
                array("checkbox"),
            ),
        )
    )
);
